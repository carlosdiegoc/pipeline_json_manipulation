#!/bin/bash

# Recebe o arquivo de log e extrai o valor do ID usando uma regex
log_file="$1"
novo_valor_groupId="$2"

release_definition_line=$(grep -o 'releaseDefinition={id=[^}]*}' "$log_file")
regex='id=([0-9a-fA-F-]+)'

if [[ $release_definition_line =~ $regex ]]; then
  novo_valor_instanceId="${BASH_REMATCH[1]}"
else
  echo "Não foi possível extrair o valor utilizando a regex fornecida."
  exit 1
fi

apt-get update
apt-get install -y jq

# Atualiza o valor no arquivo JSON usando jq
jq --arg id "$novo_valor_instanceId" --arg groupId "$novo_valor_groupId" \
  '.instanceId = $id | .groupId = $groupId' payload.json > tmp.json
mv tmp.json payload.json
